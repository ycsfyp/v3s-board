## 硬件说明

渲染图

![v3s](https://gitee.com/ycsfyp/v3s-board/raw/master/picture/v3s.jpg)

实物图

![v3s_real](https://gitee.com/ycsfyp/v3s-board/raw/master/picture/v3s_real.jpg)

+ SPI显示屏：ILI9341
+ 音频放大：PAM8403
+ 触摸：NS2009
+ wifi：rtl8723
+ 摄像头：ov2640

## 系统版本说明

内核版本5.10.0

文件系统使用buildroot2017.08构建

## 功能测试

root目录下有5个文件夹,用于测试功能，如下：

```shell
# ls -hl
total 20
drwxr-xr-x    2 root     root        4.0K Mar  5  2021 audio
drwxr-xr-x    2 root     root        4.0K Jan  1 00:01 camrea
drwxr-xr-x    2 root     root        4.0K Mar  5  2021 nes
drwxr-xr-x    2 root     root        4.0K Mar  5  2021 video
drwxr-xr-x    4 root     root        4.0K Mar  5  2021 wifi
```

测试前进入对应的目录

### audio测试

播放mp3

```shell
mpg123 6993161CY71.mp3
```

录音

```shell
./recording.sh 3
```

播放

```shell
aplay tmp.wav
```

### camrea测试

照相

```shell
./take_picture.sh
```

查看图片,f适合屏幕，m旋转

```shell
fbv test.jpg
```

录像

```shell
./take_video.sh
```

播放录像

```shell
mplayer test.avi
```

### nes测试

nes键位对应键盘**qwas**及**方向键**，由于输入程序我没改对，键盘可用但按键不灵。

```shell
./InfoNes "hdl.nes"
```

### video测试

```shell
mplayer bad_apple_30.mp4
```

### wifi测试

> *注意！*
>
> 测试wifi时要拔掉摄像头，信号很强的wifi才连的上
>
> 在buildroot2017下编译**wpa_supplicant**报错，在buildroot2020下编译无错。所以**wpa_supplicant**是buildroot2020下的复制过来的。

执行以下命令，把**wpa_supplicant**、依赖库、firmware复制到正确目录下

```shell
./config_wifi_env.sh
```

自行修改**wpa_supplicant.conf**后连接wifi

```shell
./connect_wifi.sh
```

### 触摸屏测试

```shell
ts_calibrate
ts_test
```

